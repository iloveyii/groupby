Instructions
Welcome to the Looklet front-end take-home test!
The challenge
The objective is to visualize a JSON structure, with the ability to change the grouping of objects dynamically.

We want you to create an application, with three buttons and a text-output area. When a button is clicked, the datastructure shown should be grouped by that field, and sorted by that fields value.

Source structure:
[
    {"timestamp": 1505278252166, "product": "Apple", "customer": "John Smith"},
    {"timestamp": 1505278252166, "product": "Apple", "customer": "Jane Doe"},
    {"timestamp": 1505278282166, "product": "Pear", "customer": "Will Smith"},
    {"timestamp": 1505278282166, "product": "Pear", "customer": "Some Smith"},
    {"timestamp": 1505278282166, "product": "Potato", "customer": "Anne Smith"},
    {"timestamp": 1505298282166, "product": "Potato", "customer": "Lisa Smith"},
    {"timestamp": 1505298282166, "product": "Carrot", "customer": "Eric Smith"},
]
  
End result example
Example of what the result could look like (where the 'group by product' button has been clicked)

Note: The keys should be sorted, the key order for products in the end result should be ordered "Apple", "Carrot", "Pear", and "Potato".
Group by product  Group by timestamp  Group by customer
{
    "Apple": [
        {"timestamp": 1505278252166, "product": "Apple", "customer": "John Smith"},
        {"timestamp": 1505278252166, "product": "Apple", "customer": "Jane Doe"},
    ],
    "Carrot": [
        {"timestamp": 1505298282166, "product": "Carrot", "customer": "Eric Smith"},
    ],
    "Pear": [
        {"timestamp": 1505278282166, "product": "Pear", "customer": "Will Smith"},
        {"timestamp": 1505278282166, "product": "Pear", "customer": "Some Smith"},
    ],
    "Potato": [
        {"timestamp": 1505298282166, "product": "Potato", "customer": "Anne Smith"},
        {"timestamp": 1505298282166, "product": "Potato", "customer": "Lisa Smith"},
    ],
}
      
Data grouper (your working area)
Change me!
