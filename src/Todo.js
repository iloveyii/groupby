import React, { Component } from 'react';


class Todolist extends Component {
    render() {
        return(
            <ul className="list-group">
                {this.props.items.map(item => (
                    <li className="list-group-item" key={item.id}> {item.text} </li>
                ))}
            </ul>
        );
    }
}


class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {items: [], text: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({text: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        if( ! this.state.text.length) {
            return;
        }
        const newItem = {
            text: this.state.text,
            id: Date.now()
        };

        this.setState({
            items: this.state.items.concat(newItem),
            text: ''
        })
    }

    render() {
        return (
            <div className="container">
                <div className="App-intro">
                    <br/>
                    <h3>ToDo</h3>
                    <Todolist items={this.state.items}/>
                    <hr/>
                    <form onSubmit={this.handleSubmit} className="form-inline">
                        <div className="form-group">
                            <input type="text"
                                   id="new-todo"
                                   className="form-control"
                                   onChange={this.handleChange}
                                   value={this.state.text}
                                   placeholder="What needs to be done ?"
                            />
                            &nbsp;

                            <button className="btn btn-primary">Add</button>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}

export default Todo;