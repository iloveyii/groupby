import React, { Component } from 'react';

const defaultProps = {
    groupBy: 'product'
};

class DataGrouper extends Component {
    constructor(props) {
        super(props);
        this.byProduct = this.byProduct.bind(this);
        this.byTimestamp = this.byTimestamp.bind(this);
        this.byCustomer = this.byCustomer.bind(this);

        this.state = {
            data :
                [
                    {"timestamp": 1505278252166, "product": "Apple", "customer": "John Smith"},
                    {"timestamp": 1505278252166, "product": "Apple", "customer": "Jane Doe"},
                    {"timestamp": 1505278282166, "product": "Pear", "customer": "Will Smith"},
                    {"timestamp": 1505278282166, "product": "Pear", "customer": "Some Smith"},
                    {"timestamp": 1505278282166, "product": "Potato", "customer": "Anne Smith"},
                    {"timestamp": 1505298282166, "product": "Potato", "customer": "Lisa Smith"},
                    {"timestamp": 1505298282166, "product": "Carrot", "customer": "Eric Smith"},
                ],
            grouped: {
            },
            groupBy: ''
        };
    }

    componentDidMount() {
        this.groupBy(this.props.groupBy); // default
    }

    byProduct() {
        console.log('By product clicked!');
        this.groupBy('product');
    }

    byTimestamp() {
        console.log('By Timestamp clicked!');
        this.groupBy('timestamp');
    }

    byCustomer() {
        console.log('By Customer clicked!');
        this.groupBy('customer');
    }

    groupBy(groupBy) {
        let data = this.state.data.reduce(function (p1, p2) {
            (p1[p2[groupBy]] = p1[p2[groupBy]] || []).push(p2);
            return p1;
        }, {});
        let ordered = {};
        Object.keys(data).sort().forEach(function (key) {
            ordered[key] = data[key]
        });
        this.setState({
            grouped: ordered,
            groupBy: groupBy
        });
    }

    render() {
        return(
            <div>
                <button className="btn btn-info" onClick={this.byProduct}>Group by product</button>
                <button className="btn btn-success" onClick={this.byTimestamp}>Group by timestamp</button>
                <button className="btn btn-primary" onClick={this.byCustomer}>Group by customer</button>

                <a className="btn btn-warn float-right" href="/react-kxmi89.zip">Download code</a>
                <hr/>
                <h3>Group by: {this.state.groupBy}</h3>
                <pre>{ JSON.stringify(this.state.grouped, null, 2) }</pre>
            </div>
        );
    }
}

DataGrouper.defaultProps = defaultProps;

export default DataGrouper;